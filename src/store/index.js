import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    publishingList: [
      {
        img: "nsmall.png",
        name: "ns홈쇼핌 모바일",
        url: "https://mw.nsmall.com/store/home",
        skill: "HTML, SCSS",
      },
      {
        img: "twodoorcinemaclub.jpg",
        name: "투도어씨네마클럽",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/twodoorcinemaclub/main",
        skill: "HTML, CSS",
      },
      {
        img: "pantech.jpg",
        name: "팬택",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/pantech/main.html",
        skill: "HTML, CSS",
      },
      {
        img: "murrayallan.jpg",
        name: "머레이알란",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/murrayallan/main.html",
        skill: "HTML, CSS",
      },
      {
        img: "taetea_main.jpg",
        name: "대익차",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/taetea/main.html",
        skill: "HTML, CSS",
      },
      {
        img: "taetea_content.jpg",
        name: "대익차 상품페이지",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/taetea/about.html",
        skill: "HTML, CSS",
      },
      {
        img: "calatocs.jpg",
        name: "깔라톡스",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/calatocs/main.html",
        skill: "HTML, CSS",
      },
      {
        img: "namdo.jpg",
        name: "남도여행알뜰관광",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/namdo/index.html",
        skill: "HTML, CSS",
      },
    ],
    frontendList: [
      {
        img: "cocobank.png",
        name: "cocobank",
        url: "https://cocobnk.com/home",
        skill: "HTML, SCSS, JavaScript, VueJS",
      },
      {
        img: "movach.jpg",
        name: "영화제목 검색페이지",
        url: "https://movach-5b6e4.web.app/",
        skill: "HTML, CSS, JavaScript, VueJS",
      },
      {
        img: "waffle.jpg",
        name: "학원 프로젝트",
        url:
          "https://serene-liskov-e0ba39.netlify.app/portfolio_file/waffle/main.html",
        skill: "HTML, CSS, JavaScript",
      },
      {
        img: "tdcc_join.jpg",
        name: "투도어씨네마클럽 가입페이지",
        url:
          "https://thirsty-shockley-ea31aa.netlify.app/portfolio_file/twodoorcinemaclub/join",
        skill: "HTML, CSS, JavaScript",
      },
    ],
    popupOpen: false,
    popupList: [],
  },
  mutations: {
    updatePopup(state, payload) {
      state.popupOpen = payload;
    },
    updateList(state, payload) {
      state.popupList = payload;
    },
  },
  actions: {},
  modules: {},
});
