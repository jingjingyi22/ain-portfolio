import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Project from "@/views/ProjectView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/project/:kind",
    name: "Project",
    component: Project,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
